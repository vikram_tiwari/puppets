gcloud compute instances create --preemptible --custom-cpu=1 --custom-memory=1 --zone=us-central1-c --metadata-from-file shutdown-script=./shutdown.sh --metadata-from-file startup-script=./startup.sh puppets
gcloud compute ssh --zone=us-central1-c puppets
git clone https://vikram_tiwari@bitbucket.org/vikram_tiwari/puppets.git
cd puppets
yarn
cd website-visits
pm2 start visit-website.js
