const puppeteer = require('puppeteer')

async function visit (url) {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  })
  const page = await browser.newPage()
  await page.goto(url)

  setTimeout(async() => {
    await page.screenshot({
      path: 'snaphot.png'
    })
    await browser.close()
    return true
  }, 1000 * 60 * 60 * 5)
}

setInterval(async() => {
  visit(process.env.URL || 'https://vikramtiwari.com')
}, 1000 * 61 * 60 * 5)
